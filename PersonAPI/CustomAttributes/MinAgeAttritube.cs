﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PersonAPI.CustomAttributes
{
    public class MinAgeAttritube
    {
        public class MinAgeAttribute : ValidationAttribute
        {
            int _minimumAge;

            public MinAgeAttribute(int minimumAge)
            {
                _minimumAge = minimumAge;
            }

            public override bool IsValid(object value)
            {
                DateTime dateTime;
                if (value == null)
                    return false;
                if (DateTime.TryParse(value.ToString(), out dateTime))
                {
                    return dateTime.AddYears(_minimumAge) < DateTime.Now;
                }

                return false;
            }
        }
    }
}
