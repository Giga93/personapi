﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer;
using DBLayer.Models;
using DBLayer.Models.Report;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PersonAPI.ActionFilters;
using PersonAPI.Helpers;
using PersonAPI.Models;
namespace PersonAPI.Controllers
{
    [Route("api/[controller]")]
    // [ApiController]
    [InvalidModelActionFilter]
    [Produces("application/json")]
    public class PeopleController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public static IWebHostEnvironment webHostEnvironment;
        public PeopleController(IUnitOfWork unitOfWork, IWebHostEnvironment environment, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            webHostEnvironment = environment;
        }
        // GET: api/People
        [HttpGet]

        public IActionResult Get()
        {
            IEnumerable<PersonDTO> persons = _mapper.Map<IEnumerable<PersonDTO>>(_unitOfWork.People.GetAll());

            return Ok(persons);
        }

        /// <summary>
        /// Get PersonFullInfo with id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var emp = _unitOfWork.People.Get(id);
            // _mapper.Map<PersonDTO>();
            if (emp == null)
            {
                return NotFound("Person with this id doesn`t exist");
            }
            return Ok(_mapper.Map<PersonDTO>(emp));
        }

        /// <summary>
        /// add Person full info without image upload, but can input relative path
        /// </summary>
        /// <param name="p"></param>
        [HttpPost("add")]
        public IActionResult Add([FromBody] PersonDTO p)
        {
            Person person = _mapper.Map<Person>(p);
            int result = _unitOfWork.People.AddPerson(person);
            if (result == -1)
                return BadRequest("Provided City.Id doesn`t exist!");
            if (result == -2)
                return BadRequest("Provided PhoneTypeId doesn`t exist!");
            if (result == 1)
            {
                _unitOfWork.Complete();

            }
            return Ok();

        }
        /// <summary>
        /// Person image Upload/Update
        /// Saves file and updates relative path
        /// </summary>
        /// <param name="fileUpload">Choose image file</param>
        /// <param name="id">personID</param>

        [HttpPost("addPersonImage/{id}")]
        public void AddPersonImage([FromForm]FileUpload fileUpload, int id)
        {
            string url = "";
            if (fileUpload.files.Length > 0)
            {
                if (!Directory.Exists(webHostEnvironment.ContentRootPath + "\\UploadedFiles\\"))
                {
                    Directory.CreateDirectory(webHostEnvironment.ContentRootPath + "\\UploadedFiles\\");
                }
                using (FileStream fStream = System.IO.File.Create(webHostEnvironment.ContentRootPath + "\\UploadedFiles\\" + fileUpload.files.FileName))
                {
                    fileUpload.files.CopyTo(fStream);
                    fStream.Flush();
                    url = "\\UploadedFiles\\" + fileUpload.files.FileName;
                }
                _unitOfWork.People.AddPersonImage(id, url);
                _unitOfWork.Complete();
            }

        }
        /// <summary>
        /// adds person relations object
        /// </summary>
        /// <param name="personRelation"></param>
        [HttpPost("addPersonRelation")]
        public IActionResult AddPersonRelation([FromBody] PersonRelationDTO personRelation)
        {
            RelationType relType = _mapper.Map<RelationType>(personRelation.RelationType);
            var result = _unitOfWork.People.AddPersonRelation(personRelation.PersonFromId, personRelation.PersonToId, relType);
            if (result == -3)
            {
                return BadRequest("Connection between these two person already exists!");
            }
            if (result == 1)
            {
                _unitOfWork.Complete();
            }
            return Ok();

        }

        /// <summary>
        /// quickSearch with paging
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="LastName"></param>
        /// <param name="PrivateNumber"></param>
        /// <param name="currentPage"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        [HttpGet("quickSearch")]
        public IActionResult GetQuickSearchResults(string Name, string LastName, string PrivateNumber, int currentPage = 0, int itemsPerPage = 0)
        {
            var persons = _unitOfWork.People.Search(currentPage, itemsPerPage,
                x => (string.IsNullOrEmpty(Name) || x.Name.Contains(Name)) &&
                (string.IsNullOrEmpty(LastName) || x.LastName.Contains(LastName)) &&
                (string.IsNullOrEmpty(PrivateNumber) || x.PrivateNumber.Contains(PrivateNumber)));
            if (persons.Count() == 0)
            {
                return NotFound("Persons with such filter don`t exist");
            }
            return Ok(persons);
        }
        /// <summary>
        /// Full Search with paging
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="LastName"></param>
        /// <param name="PrivateNumber"></param>
        /// <param name="Gender"></param>
        /// <param name="CityName"></param>
        /// <param name="telephoneNumber"></param>
        /// <param name="currentPage"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        [HttpGet("fullSearch")]
        public IActionResult GetFullSearchResults(string Name, string LastName, string PrivateNumber,
            string Gender, string CityName, string telephoneNumber, int currentPage = 0, int itemsPerPage = 0)
        {

            var persons = _unitOfWork.People.Search(currentPage, itemsPerPage,
                x =>
                (string.IsNullOrEmpty(Name) || x.Name.Contains(Name)) &&
                (string.IsNullOrEmpty(LastName) || x.LastName.Contains(LastName)) &&
                (string.IsNullOrEmpty(PrivateNumber) || x.PrivateNumber.Contains(PrivateNumber)) &&
                (string.IsNullOrEmpty(Gender) || x.Gender.Contains(Gender)) &&
                (string.IsNullOrEmpty(CityName) || x.City.Name.Contains(CityName)) &&
                (string.IsNullOrEmpty(telephoneNumber) || x.PhoneNumbers.Any(y => y.Number.Contains(telephoneNumber)))
                );
            if (persons.Count() == 0)
            {
                return NotFound("Persons with such filter don`t exist");
            }
            return Ok(persons);

        }
        /// <summary>
        /// Get Report Data for person total relations per relationType
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetPersonRelationsReport")]
        public IActionResult GetPersonRelationsReport()
        {
            HelperMethods hm = new HelperMethods();
            var report = hm.MapPersonRelationsReportDictionary(_mapper,
             _unitOfWork.People.GetPersonRelationsReport().ToList());
            if (report.Count() == 0)
            {
                return NotFound("No People Exist!!!");
            }
            return Ok(report);
        }

        /// <summary>
        /// Edit People main data
        /// </summary>
        /// <param name="id"></param>
        /// <param name="personDTO"></param>
        // PUT: api/People/5
        [HttpPost("edit")]
        public IActionResult EditPerson([FromBody]PersonDTO personDTO)
        {
            var person = _mapper.Map<Person>(personDTO);
            int result = _unitOfWork.People.EditPerson(person);
            if (result == -1)
                return BadRequest("Provided City.Id doesn`t exist!");
            if (result == -2)
                return BadRequest("Provided PhoneTypeId doesn`t exist!");
            if (result == 1)
            {
                _unitOfWork.Complete();

            }
            return Ok();
        }
        /// <summary>
        /// Delete people with relations
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Person p = new Person { Id = id };
            _unitOfWork.People.Remove(p);
            _unitOfWork.Complete();
        }
        /// <summary>
        /// delete relation using its record id
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("personRelation/{personFromId}/{personToId}")]
        public IActionResult DeletePersonRelation(int personFromId, int personToId)
        {
            int result = _unitOfWork.People.RemovePersonRelation(personFromId, personToId);
            if (result == -4)
            {
                return BadRequest("Provided Relation doesn`t exist!");
            }
            if (result == 1)
            {
                _unitOfWork.Complete();
            }
            return Ok();
        }
    }
}