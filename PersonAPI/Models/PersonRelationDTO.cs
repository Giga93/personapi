﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonAPI.Models
{
    public class PersonRelationDTO
    {
        public RelationTypeDTO RelationType { get; set; }
        public int PersonFromId { get; set; }
        public int PersonToId { get; set; }
        public PersonDTO PersonFrom { get; set; }
        public PersonDTO PersonTo { get; set; }
        public int Count { get; set; }
    }
}
