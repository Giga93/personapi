﻿using PersonAPI.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static PersonAPI.CustomAttributes.MinAgeAttritube;

namespace PersonAPI.Models
{
    public class PersonDTO
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        [MinLength(2, ErrorMessage = "Name length can`t be less than 2")]
        [MaxLength(50, ErrorMessage = "Name length can`t be greater than 50")]
        [RegularExpression("(^[A-Za-z]*$)|(^[ა-ჰ]*$)", ErrorMessage = "Use only Latin or Georgian Characters separately")]
        public string Name { get; set; }
        [Required(ErrorMessage = "LastName is Required")]
        [MinLength(2, ErrorMessage = "LastName length can`t be less than 2")]
        [MaxLength(50, ErrorMessage = "LastName length can`t be greater than 50")]
        [RegularExpression("(^[A-Za-z]*$)|(^[ა-ჰ]*$)", ErrorMessage = "Use only Latin or Georgian Characters separately")]
        public string LastName { get; set; }
        [StringRange(AllowableValues = new[] { "ქალი", "კაცი" }, ErrorMessage = "Gender must be either 'ქალი' or 'კაცი'.")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Private Number is Required")]
        [MaxLength(11, ErrorMessage = "11 Characters Required"), MinLength(11, ErrorMessage = "11 Characters required")]
        public string PrivateNumber { get; set; }
        [Required]
        [MinAge(18, ErrorMessage = "Minimal Age must be 18")]
        public DateTime? BirthDate { get; set; }
        public string Photo { get; set; }
        public CityDTO City { get; set; }
        public List<PhoneNumberDTO> PhoneNumbers { get; set; }
        public List<PersonRelationDTO> RelatedToPersons { get; set; }
        public List<PersonRelationDTO> RelatedFromPersons { get; set; }
    }
}
