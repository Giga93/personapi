﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PersonAPI.Models
{
    public class PhoneNumberDTO
    {
        public int Id { get; set; }
        [MinLength(2, ErrorMessage = "Number length can`t be less than 4")]
        [MaxLength(50, ErrorMessage = "Number length can`t be greater than 50")]
        public string Number { get; set; }
        public PhoneNumberTypeDTO PhoneNumberType { get; set; }
    }
}
