﻿using DBLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonAPI.Models
{
    public class PersonRelationsReportDTO
    {
        public Person person { get; set;}
        public List<PersonRelationDTO> relations { get; set; }
    }
}
