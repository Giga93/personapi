﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonAPI.Models
{
    public class RelationTypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
