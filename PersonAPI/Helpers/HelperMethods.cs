﻿using AutoMapper;
using DBLayer.Models.Report;
using PersonAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonAPI.Helpers
{
    public class HelperMethods
    {
        public IEnumerable<PersonRelationsReportDTO> MapPersonRelationsReportDictionary(IMapper _mapper,List<PersonRelationsReport> list)
        {
            var list1 = new List<PersonRelationsReportDTO>();
            foreach (var l in list)
            {
                var prrDTO = new PersonRelationsReportDTO();
                prrDTO.person = l.person;
                //prrDTO.person.RelatedPersons = null;
                prrDTO.relations = new List<PersonRelationDTO>();
                foreach (var k in l.keyValuePairs)
                {

                    PersonRelationDTO prDTO = new PersonRelationDTO
                    {
                        RelationType = _mapper.Map<RelationTypeDTO>(k.Key),
                        Count = k.Value
                    };
                    prrDTO.relations.Add(prDTO);
                }
                list1.Add(prrDTO);
            }
            return list1;
        }
    }
}
