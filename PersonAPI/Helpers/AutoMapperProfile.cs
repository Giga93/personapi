﻿using AutoMapper;
using DBLayer.Models;
using PersonAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<PersonDTO, Person>();
            CreateMap<Person, PersonDTO>();

            CreateMap<CityDTO, City>();
            CreateMap<City, CityDTO>();

            CreateMap<PersonRelationDTO, PersonRelation>();
            CreateMap<PersonRelation, PersonRelationDTO>();

            CreateMap<PhoneNumberDTO, PhoneNumber>();
            CreateMap<PhoneNumber, PhoneNumberDTO>();

            CreateMap<PhoneNumberTypeDTO, PhoneNumberType>();
            CreateMap<PhoneNumberType, PhoneNumberTypeDTO>();

            CreateMap<RelationTypeDTO, RelationType>();
            CreateMap<RelationType, RelationTypeDTO>();

        }
    }

}
