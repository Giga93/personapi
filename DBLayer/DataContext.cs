﻿using DBLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBLayer
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> opts) : base(options: opts) { }
        public DbSet<Person> People { get; set; }
        public DbSet<PersonRelation> PersonRelations { get; set; }
        public DbSet<RelationType> RelationTypes { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public DbSet<PhoneNumberType> PhoneNumberTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PersonRelation>()
                .HasKey(u => new { u.PersonFromId, u.PersonToId });

            modelBuilder.Entity<Person>()
                .HasMany(i => i.RelatedToPersons)
                .WithOne(c => c.PersonFrom)
                .HasForeignKey(c => c.PersonFromId)
                .OnDelete(DeleteBehavior.NoAction);



            modelBuilder.Entity<Person>()
                .HasMany(i => i.RelatedFromPersons)
                .WithOne(c => c.PersonTo)
                .HasForeignKey(c => c.PersonToId)
                .OnDelete(DeleteBehavior.NoAction); 
            



            modelBuilder.Entity<PhoneNumber>()
            .HasOne(i => i.Person)
            .WithMany(c => c.PhoneNumbers)
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<City>().HasData(
                new City { Id = 1, Name = "თბილისი" },
                new City { Id = 2, Name = "ქუთაისი" },
                new City { Id = 3, Name = "ბათუმი" },
                new City { Id = 4, Name = "თელავი" },
                new City { Id = 5, Name = "ზუგდიდი" },
                new City { Id = 6, Name = "რუსთავი" }
                );

            modelBuilder.Entity<PhoneNumberType>().HasData(
                new PhoneNumberType { Id = 1, Name = "მობილური" },
                new PhoneNumberType { Id = 2, Name = "ოფისის" },
                new PhoneNumberType { Id = 3, Name = "სახლის" }
                );

            modelBuilder.Entity<RelationType>().HasData(
                new RelationType { Id = 1, Name = "კოლეგა" },
                new RelationType { Id = 2, Name = "ნაცნობი" },
                new RelationType { Id = 3, Name = "ნათესავი" },
                new RelationType { Id = 4, Name = "სხვა" }
                );
        }
    }
}

