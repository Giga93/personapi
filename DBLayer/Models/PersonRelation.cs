﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBLayer.Models
{
    public class PersonRelation
    {
        public RelationType RelationType { get; set; }
        public int PersonFromId { get; set; }
        public int PersonToId { get; set; }
        public Person PersonFrom { get; set; }
        public Person PersonTo { get; set; }
        
    }
}
