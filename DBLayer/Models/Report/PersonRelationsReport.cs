﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBLayer.Models.Report
{
    public class PersonRelationsReport
    {
        [NotMapped]
        public Person person { get; set; }
        public Dictionary<RelationType, int> keyValuePairs { get; set; }
    }
}
