﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBLayer.Models
{
    public class Person
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string LastName { get; set; }
        public string Gender { get; set; }
        [Required]
        [MaxLength(11),MinLength(11)]
        public string PrivateNumber { get; set; }
        [Required]
        public DateTime? BirthDate { get; set; }
        public string Photo { get; set; }
        public City City { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }
        public virtual List<PersonRelation> RelatedToPersons { get; set; }
        public virtual List<PersonRelation> RelatedFromPersons { get; set; }
    }
}
