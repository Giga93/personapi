﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBLayer.Models
{
    public class RelationType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
