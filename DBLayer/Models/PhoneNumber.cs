﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DBLayer.Models
{
    public class PhoneNumber
    {
        [Key]
        public int Id { get; set; }
        [MinLength(2, ErrorMessage = "Number length can`t be less than 4")]
        [MaxLength(50, ErrorMessage = "Number length can`t be greater than 50")]
        public string Number { get; set; }
        public PhoneNumberType PhoneNumberType { get; set; }
        public Person Person { get; set; } 
    }
}
