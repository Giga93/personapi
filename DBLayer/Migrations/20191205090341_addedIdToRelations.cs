﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBLayer.Migrations
{
    public partial class addedIdToRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonFromId",
                table: "PersonRelations");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonToId",
                table: "PersonRelations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations",
                columns: new[] { "Id", "PersonFromId", "PersonToId" });

            migrationBuilder.CreateIndex(
                name: "IX_PersonRelations_PersonFromId",
                table: "PersonRelations",
                column: "PersonFromId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonFromId",
                table: "PersonRelations",
                column: "PersonFromId",
                principalTable: "People",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonToId",
                table: "PersonRelations",
                column: "PersonToId",
                principalTable: "People",
                principalColumn: "Id");


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonFromId",
                table: "PersonRelations");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonToId",
                table: "PersonRelations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations");

            migrationBuilder.DropIndex(
                name: "IX_PersonRelations_PersonFromId",
                table: "PersonRelations");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations",
                columns: new[] { "PersonFromId", "PersonToId" });

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonFromId",
                table: "PersonRelations",
                column: "PersonFromId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonToId",
                table: "PersonRelations",
                column: "PersonToId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
