﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBLayer.Migrations
{
    public partial class sampleDataAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_People_City_CityId",
                table: "People");

            migrationBuilder.DropForeignKey(
                name: "FK_PhoneNumber_People_PersonId",
                table: "PhoneNumber");

            migrationBuilder.DropForeignKey(
                name: "FK_PhoneNumber_PhoneNumberType_PhoneNumberTypeId",
                table: "PhoneNumber");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneNumberType",
                table: "PhoneNumberType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneNumber",
                table: "PhoneNumber");

            migrationBuilder.DropPrimaryKey(
                name: "PK_City",
                table: "City");

            migrationBuilder.RenameTable(
                name: "PhoneNumberType",
                newName: "PhoneNumberTypes");

            migrationBuilder.RenameTable(
                name: "PhoneNumber",
                newName: "PhoneNumbers");

            migrationBuilder.RenameTable(
                name: "City",
                newName: "Cities");

            migrationBuilder.RenameIndex(
                name: "IX_PhoneNumber_PhoneNumberTypeId",
                table: "PhoneNumbers",
                newName: "IX_PhoneNumbers_PhoneNumberTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_PhoneNumber_PersonId",
                table: "PhoneNumbers",
                newName: "IX_PhoneNumbers_PersonId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneNumberTypes",
                table: "PhoneNumberTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneNumbers",
                table: "PhoneNumbers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cities",
                table: "Cities",
                column: "Id");

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "თბილისი" });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "ქუთაისი" });

            migrationBuilder.AddForeignKey(
                name: "FK_People_Cities_CityId",
                table: "People",
                column: "CityId",
                principalTable: "Cities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumbers_People_PersonId",
                table: "PhoneNumbers",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumbers_PhoneNumberTypes_PhoneNumberTypeId",
                table: "PhoneNumbers",
                column: "PhoneNumberTypeId",
                principalTable: "PhoneNumberTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_People_Cities_CityId",
                table: "People");

            migrationBuilder.DropForeignKey(
                name: "FK_PhoneNumbers_People_PersonId",
                table: "PhoneNumbers");

            migrationBuilder.DropForeignKey(
                name: "FK_PhoneNumbers_PhoneNumberTypes_PhoneNumberTypeId",
                table: "PhoneNumbers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneNumberTypes",
                table: "PhoneNumberTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneNumbers",
                table: "PhoneNumbers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cities",
                table: "Cities");

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Cities",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.RenameTable(
                name: "PhoneNumberTypes",
                newName: "PhoneNumberType");

            migrationBuilder.RenameTable(
                name: "PhoneNumbers",
                newName: "PhoneNumber");

            migrationBuilder.RenameTable(
                name: "Cities",
                newName: "City");

            migrationBuilder.RenameIndex(
                name: "IX_PhoneNumbers_PhoneNumberTypeId",
                table: "PhoneNumber",
                newName: "IX_PhoneNumber_PhoneNumberTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_PhoneNumbers_PersonId",
                table: "PhoneNumber",
                newName: "IX_PhoneNumber_PersonId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneNumberType",
                table: "PhoneNumberType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneNumber",
                table: "PhoneNumber",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_City",
                table: "City",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_People_City_CityId",
                table: "People",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_People_PersonId",
                table: "PhoneNumber",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_PhoneNumberType_PhoneNumberTypeId",
                table: "PhoneNumber",
                column: "PhoneNumberTypeId",
                principalTable: "PhoneNumberType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
