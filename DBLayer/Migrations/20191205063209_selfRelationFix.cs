﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBLayer.Migrations
{
    public partial class selfRelationFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonId",
                table: "PersonRelations");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_RelatedPersonId",
                table: "PersonRelations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations");

            migrationBuilder.DropIndex(
                name: "IX_PersonRelations_PersonId",
                table: "PersonRelations");

            migrationBuilder.DropIndex(
                name: "IX_PersonRelations_RelatedPersonId",
                table: "PersonRelations");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "PersonRelations");

            migrationBuilder.DropColumn(
                name: "RelatedPersonId",
                table: "PersonRelations");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "People");


            migrationBuilder.AddColumn<int>(
                name: "PersonFromId",
                table: "PersonRelations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PersonToId",
                table: "PersonRelations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_PersonRelations_Id",
                table: "PersonRelations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations",
                columns: new[] { "PersonFromId", "PersonToId" });

            migrationBuilder.CreateIndex(
                name: "IX_PersonRelations_PersonToId",
                table: "PersonRelations",
                column: "PersonToId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonFromId",
                table: "PersonRelations",
                column: "PersonFromId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonToId",
                table: "PersonRelations",
                column: "PersonToId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonFromId",
                table: "PersonRelations");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonToId",
                table: "PersonRelations");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_PersonRelations_Id",
                table: "PersonRelations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations");

            migrationBuilder.DropIndex(
                name: "IX_PersonRelations_PersonToId",
                table: "PersonRelations");

            migrationBuilder.DropColumn(
                name: "PersonFromId",
                table: "PersonRelations");

            migrationBuilder.DropColumn(
                name: "PersonToId",
                table: "PersonRelations");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "PersonRelations",
                type: "int",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "PersonRelations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RelatedPersonId",
                table: "PersonRelations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "People",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PersonRelations",
                table: "PersonRelations",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_PersonRelations_PersonId",
                table: "PersonRelations",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonRelations_RelatedPersonId",
                table: "PersonRelations",
                column: "RelatedPersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonId",
                table: "PersonRelations",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_RelatedPersonId",
                table: "PersonRelations",
                column: "RelatedPersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
