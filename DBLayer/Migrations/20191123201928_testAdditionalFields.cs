﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBLayer.Migrations
{
    public partial class testAdditionalFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonId",
                table: "PersonRelations");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_RelatedPersonId",
                table: "PersonRelations");

            migrationBuilder.AlterColumn<int>(
                name: "RelatedPersonId",
                table: "PersonRelations",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PersonId",
                table: "PersonRelations",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonId",
                table: "PersonRelations",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_RelatedPersonId",
                table: "PersonRelations",
                column: "RelatedPersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_PersonId",
                table: "PersonRelations");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonRelations_People_RelatedPersonId",
                table: "PersonRelations");

            migrationBuilder.AlterColumn<int>(
                name: "RelatedPersonId",
                table: "PersonRelations",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PersonId",
                table: "PersonRelations",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_PersonId",
                table: "PersonRelations",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonRelations_People_RelatedPersonId",
                table: "PersonRelations",
                column: "RelatedPersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
