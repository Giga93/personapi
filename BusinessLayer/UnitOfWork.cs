﻿using BusinessLayer.Implementations;
using BusinessLayer.Repositories;
using DBLayer;

namespace BusinessLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        public IPersonRepository People { get; set; }
        public IRelationTypesRepository RelationTypes { get; set; }
        private readonly DataContext _context;

        public UnitOfWork(DataContext context)
        {
            _context = context;
            People = new PersonRepository(_context);
            RelationTypes = new RelationTypesRepository(_context);
        }
        public int Complete()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
