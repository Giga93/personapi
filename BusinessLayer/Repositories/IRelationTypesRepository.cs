﻿using DBLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Repositories
{
    public interface IRelationTypesRepository:IRepository<RelationType>
    {
    }
}
