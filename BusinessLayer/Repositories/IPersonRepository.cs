﻿using DBLayer.Models;
using DBLayer.Models.Report;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BusinessLayer.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
        public int AddPerson(Person person);
        public int EditPerson(Person person);
        public IEnumerable<Person> Search(int currentPage, int itemsPerPage, Expression<Func<Person, bool>> predicate);
        public int AddPersonRelation(int p1, int p2, RelationType relType);
        public int RemovePersonRelation(int personFromId, int personToId);
        public IEnumerable<PersonRelationsReport> GetPersonRelationsReport();
        public void AddPersonImage(int personId, string filePath);
    }
}
