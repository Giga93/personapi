﻿using BusinessLayer.Repositories;
using DBLayer;
using DBLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Implementations
{
    public class RelationTypesRepository:Repository<RelationType>,IRelationTypesRepository
    {
        public RelationTypesRepository(DataContext context) : base(context) { }
    }
}
