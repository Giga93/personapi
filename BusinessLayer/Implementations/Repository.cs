﻿using BusinessLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace BusinessLayer.Implementations
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DbContext _Context;
        public Repository(DbContext context)
        {
            _Context = context;
        }
        public virtual TEntity Get(int id)
        {
            return _Context.Set<TEntity>().Find(id);
        }
        public IEnumerable<TEntity> GetAll()
        {
            return _Context.Set<TEntity>().ToList();
        }
        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _Context.Set<TEntity>().Where(predicate).ToList();
        }
        public virtual void Add(TEntity entity)
        {
            _Context.Set<TEntity>().Add(entity);
        }
        public virtual void Remove(TEntity entity)
        {
            _Context.Set<TEntity>().Remove(entity);
        }
    }
}
