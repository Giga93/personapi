﻿using BusinessLayer.Repositories;
using DBLayer;
using DBLayer.Models;
using DBLayer.Models.Report;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BusinessLayer.Implementations
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(DataContext context) : base(context) { }
        public override void Add(Person p) { }
        public int AddPerson(Person p)
        {
            if (!_Context.Set<City>().Any(x => x.Id == p.City.Id))
            {
                return -1;
            }
            foreach (var pn in p.PhoneNumbers)
            {
                if (!_Context.Set<PhoneNumberType>().Any(x => x.Id == pn.PhoneNumberType.Id))
                {
                    return -2;
                }
            }
            _Context.Attach(p);
            _Context.Add(p);
            return 1;
        }


        public int EditPerson(Person p)
        {
            if (!_Context.Set<City>().Any(x => x.Id == p.City.Id))
            {
                return -1;
            }
            foreach (var pn in p.PhoneNumbers)
            {
                if (!_Context.Set<PhoneNumberType>().Any(x => x.Id == pn.PhoneNumberType.Id))
                {
                    return -2;
                }
            }
            var person = _Context.Set<Person>().Include(x => x.PhoneNumbers).Where(x => x.Id == p.Id).First();
            if (person != null)
            {
                person.Name = p.Name;
                person.LastName = p.LastName;
                person.PrivateNumber = p.PrivateNumber;
                person.Gender = p.Gender;
                person.BirthDate = p.BirthDate;
                _Context.Attach(p.City);
                person.City = p.City;
                _Context.RemoveRange(person.PhoneNumbers);

                foreach (var number in p.PhoneNumbers)
                {
                    _Context.Attach(number.PhoneNumberType);
                    number.Person = person;
                    _Context.Set<PhoneNumber>().Add(number);
                }
                _Context.Set<Person>().Update(person);
            }

            return 1;
        }

        public int AddPersonRelation(int p1, int p2, RelationType relType)
        {
            var existingRelations = _Context.Set<PersonRelation>();
            if ((existingRelations.Any(x => x.PersonFromId == p1 && x.PersonToId == p2)) ||
                (existingRelations.Any(x => x.PersonToId == p1 && x.PersonFromId == p2)))
            {
                return -3;
            }
            PersonRelation relation = new PersonRelation();
            relation.PersonFromId = p1;
            relation.PersonToId = p2;
            relation.RelationType = relType;
            _Context.Attach(relation);
            _Context.Set<PersonRelation>().Add(relation);
            return 1;
        }
        public IEnumerable<PersonRelationsReport> GetPersonRelationsReport()
        {
            var persons = GetPersonsListWithFullInfo();
            List<PersonRelationsReport> grouped = persons.Select(x => new
            PersonRelationsReport
            {
                person = x,
                keyValuePairs = x.RelatedFromPersons.GroupBy(y => y.RelationType).Select(g => new { relationType = g.Key, count = g.Count() }).ToDictionary(k => k.relationType, i => i.count),
            }).ToList();
            return grouped;
            
        }
        /// <summary>
        /// Quick Fix
        /// include child ids and objects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Person> GetPersonsListWithFullInfo()
        {
            var persons = _Context.Set<Person>().Include(x => x.City)
                            .Include(x => x.PhoneNumbers).ThenInclude(x => x.PhoneNumberType)
                            .Include(x => x.RelatedToPersons).ThenInclude(x => x.RelationType)
                            .Include(x => x.RelatedFromPersons).ToList();
            return persons.ToList();
        }
        public override Person Get(int id)
        {
            var persons = GetPersonsListWithFullInfo().Where(x => x.Id == id);
            if (persons.Count() == 1)
            {
                return persons.First();

            }
            else { return null; }


        }
        public void AddPersonImage(int personId, string filePath)
        {
            var p = new Person
            {
                Id = personId,
                Photo = filePath
            };
            _Context.Attach(p);
            _Context.Entry(p).Property(x => x.Photo).IsModified = true;

        }

        public IEnumerable<Person> Search(int currentPage, int itemsPerPage, Expression<Func<Person, bool>> predicate)
        {
           var res = _Context.Set<Person>().Include(x => x.City)
                            .Include(x => x.PhoneNumbers).ThenInclude(x => x.PhoneNumberType)
                            .Include(x => x.RelatedToPersons).ThenInclude(x => x.RelationType)
                            .Include(x => x.RelatedFromPersons)
             .Where(predicate)
             .Skip((currentPage - 1) * itemsPerPage)
             .Take(itemsPerPage);
            return res;
        }

        public override void Remove(Person p)
        {
            var relatedPersons = _Context.Set<PersonRelation>()
              .Where(x =>
                x.PersonToId == p.Id ||
                x.PersonFromId == p.Id)
              .ToList();
            _Context.RemoveRange(relatedPersons);
            _Context.Attach(p);
            _Context.Remove(p);
        }

        public int RemovePersonRelation(int PersonFromId, int PersonToId )
        {
            var existingRelations = _Context.Set<PersonRelation>();
            var relation = existingRelations.FirstOrDefault(x => (x.PersonFromId == PersonFromId && x.PersonToId == PersonToId) ||
                (x.PersonToId == PersonFromId && x.PersonFromId == PersonToId));
            if (relation == null)
            {
                return -4;
            }
            _Context.Set<PersonRelation>().Remove(relation);
            return 1;
        }
    }
}
