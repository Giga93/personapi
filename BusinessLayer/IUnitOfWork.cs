﻿using BusinessLayer.Repositories;
using System;

namespace BusinessLayer
{
    public interface IUnitOfWork : IDisposable
    {
        IPersonRepository People { get; }
        IRelationTypesRepository RelationTypes { get; }
        int Complete();
    }
}
